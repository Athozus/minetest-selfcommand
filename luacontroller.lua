--[[ Self command Lua program
By: AirSThib with sivarajan
On: January-February-March 2021 for programming event organised by sivarajan
Licence: GNU GPL 3.0
--]]

products = {
    {"fries", "French fries", "fries.png", 0.02, 0.0, 0.8},
    {"bread", "Bread", "farming_bread.png", 0.01, 0.0, 1.6},
    {"sandwich", "Sandwich", "farming_toast_sandwich.png", 0.02, 0.0, 2.4},
    {"porridge", "Porridge", "farming_porridge.png", 0.02, 0.0, 3.2},
    {"blueberryPie", "Blueberry Pie", "farming_blueberry_pie.png", 0.01, 0.0, 4.8},
    {"chocolateDonut", "Chocolate donut", "farming_donut_chocolate.png", 0.02, 0.0, 5.6},
    {"jaffaCake", "Jaffa Cake", "farming_jaffa_cake.png", 0.02, 0.0, 6.4},
    {"blueberryMuffin", "Blueberry Muffin", "farming_blueberry_muffin.png", 0.02, 0.0, 7.2}
}

workspace = {}

function buttonProduct(prod)
    local name, label, image, price, x, y = prod[1], prod[2], prod[3], prod[4], prod[5], prod[6]
    workspace[#workspace+1]={
        command="addimage",
        texture_name=image,
        X=x, W=0.8, Y=y, H=0.8,
    }
    workspace[#workspace+1] = {
        command="addbutton",
        label=label,
        name=name,
        X= x + 0.8, W=2.4, Y=y, H=0.8,
    }

    if event.type == "digiline" then
        if event.msg[name] then
            totalProducts = totalProducts + event.msg.quantity
            totalMoney = totalMoney + (price*event.msg.quantity)
            update()
        end
    end
end

function update()
    workspace = {
        {
            command="clear",
        },{
            command="addlabel",
            label="Our foods",
            X=0, W=4, Y=0, H=0.8,
        },{
            command="addlabel",
            label="Our desserts",
            X=0, W=4, Y=4, H=0.8,
        },
    }
    
    workspace[#workspace+1] = {
        command="addlabel",
        label="Total of products: " .. mem.totalProducts .. " P",
        X = 6.4, W = 4, Y = 0, H = 0.8
    }
    workspace[#workspace+1] = {
        command="addlabel",
        label="Total of money: " .. mem.totalMoney .. " AB",
        X = 6.4, W = 4, Y = 0.8, H = 0.8
    }
    workspace[#workspace+1] = {
        command="addbutton",
        label="Check command",
        name="check",
        X = 6.4, W = 3.2, Y = 1.6, H = 0.8
    }
    workspace[#workspace+1] = {
        command="addbutton",
        label="Cancel / New command",
        name="cancel",
        X = 6.4, W = 3.2, Y = 2.4, H = 0.8
    }
    workspace[#workspace+1] = {
        command="addfield",
        label="Add quantity",
        name="quantity",
        default="1",
        X = 6.4, W = 2.4, Y = 6.4, H = 0.8
    }
end

local totalProducts = 0
local totalMoney = 0

for i = 1, #products do
    buttonProduct(products[i])
end

update()

digiline_send("touch",workspace)

if event.type == "digiline" then
    --block that handles events of the component
    
    digiline_send("ter",event)
    
    if event.msg.check then
        workspace[#workspace+1] = {
            command="addlabel",
            label="Please insert " .. tostring(mem.totalMoney) .. " AB in the pipework",
            X=4.8, W = 4, Y = 3.2, H = 1.6
        }
    elseif event.msg.cancel then
        mem.totalProducts = 0
        mem.totalMoney = 0.00
    end
    
    update()
    digiline_send("touch", workspace)
elseif event.type == "program" then
    -- This block consist of initialization of touch screen
    -- mem variable initialization block
    digiline_send("touch",workspace)
end
